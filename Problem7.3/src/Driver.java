//Problem7.3
//Design and implement a class called Course that represents a course taken at a 
//school. A course object should keep track of up to five students, as 
//represented by the modified Student class from the previous programming project.
//The constructor of the Course class should accept only the name of the course. 
//Provide a method called addStudent that accepts one Student parameter (the 
//Course object should keep track of how many valid students have been added to 
//the course). Provide a method called average that computes and returns the 
//average of all student's test score averages. Provide a method called roll 
//that prints all students in the course. Create a driver class with a main 
//method that creates a course, adds several students, prints a roll, and prints 
//the overall course test average.

//inputs:
//outputs: test the class course


public class Driver 
	{

	public static void main(String[] args) 
	{
	   Address school = new Address("800 Lancaster Ave.", "Villanova",
	                                "PA", 19085);
	                        Address jHome = new Address("21 Jump Street", "Blacksburg",
	                                "VA", 24551);
	                        Student john = new Student("John", "Smith", jHome, school);

	                        Address mHome = new Address("123 Main Street", "Euclid", "OH",
	                                44132);
	                        Student marsha = new Student("Marsha", "Jones", mHome, school);

	                        System.out.println(john);
	                        System.out.println();
	                        System.out.println(marsha);
	                        
	                        Student frank = new Student("Frank", "Niscak", mHome, school, 10, 20, 30);
	                        Student joe = new Student ("joe", "nic", mHome, school, 60, 70, 80);
	                        System.out.println(frank);	

	                        
	                        
	}

}
