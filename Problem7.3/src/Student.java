//Problem7.2
//Modify the Student class presented in this chapter as follows. Each student 
//object should also contain the scores for three tests. Provide a constructor 
//that sets all instance values based on parameter values. Overload the 
//constructor such that each test score is assumed to be initially zero. 
//Provide a method called setTestScore that accepts two parameters: the test 
//number (1 through 3) and the score. Also provide a method called getTestScore 
//that accepts the test number and returns the appropriate score. Provide a 
//method called average that computes and returns the average test score for 
//this student. Modify the toString method such that the test scores and average 
//are included in the description of the student. Modify the driver class main 
//method to exercise the new Student methods.

public class Student
{
    private String firstName, lastName;
    private Address homeAddress, schoolAddress;
    private int score1 = -1, score2, score3;
    //-----------------------------------------------------------------
    //  Constructor: Sets up this student with the specified values.
    //-----------------------------------------------------------------
    public Student(String first, String last, Address home,
            Address school, int score1, int score2, int score3)
    {
        firstName = first;
        lastName = last;
        homeAddress = home;
        schoolAddress = school;
        this.score1 = score1;
        this.score2 = score2;
        this.score3 = score3;
    }
    
    public void setTestScore(int testNumber, int score)
    {
    	switch(testNumber)
    	{
    	case 1:
    		this.score1 = score;
    		break;
    	case 2:
    		this.score2 = score;
    		break;
    	case 3:
    		this.score3 = score;
    		break;
    	default:
    		break;
    	}
    }
    
    public int getTestScore(int testNumber)
    {
    	int score = -1;
    	switch(testNumber)
    	{
    	case 1:
    		score = this.score1;
    		break;
    	case 2:
    		score = this.score2;
    		break;
    	case 3:
    		score = this.score3;
    		break;
    	default:
    		break;
    	}
    	return score;
    }
    
    public double average()
    {
    	return (score1 + score2 + score3) / 3.0;
    }
    //-----------------------------------------------------------------
    //  Returns a string description of this Student object.
    //-----------------------------------------------------------------
    public Student(String first, String last, Address home,
            Address school)
    {
        firstName = first;
        lastName = last;
        homeAddress = home;
        schoolAddress = school;
        score1 = 0;
        score2 = 0;
        score3 = 0;
    }

    
    
    public String toString()
    {
        String result;

        result = firstName + " " + lastName + "\n";
        result += "Home Address:\n" + homeAddress + "\n";
        result += "School Address:\n" + schoolAddress;
        result += "\nScore 1: " +score1;
        result += "\nScore 2: " +score2;
        result += "\nScore 3: " +score3;
        result += "\nAverage: " + this.average();
        
        return result;
    }
}