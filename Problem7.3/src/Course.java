
import java.util.*;

public class Course
{
  private ArrayList<Student> students = new ArrayList<Student>();
  private final int MAX_STUDENT = 5;
  private String courseName = "";
  
  public Course(String course)
  {
    this.courseName = course;
  }
  
  public void addStudent(Student student)
  {
    if (students.size() < MAX_STUDENT)
    students.add(student);
  }
  
  public double average()
  {
    double sumOfAllAverages = 0.0;
    for (int index = 0; index < students.size(); index++)
    {
      sumOfAllAverages = sumOfAllAverages + students.get(index).average();
    }
    
    if (students.size() == 0) //no students
      return -1.0;
    
    return sumOfAllAverages / students.size();
  }
  
  public String roll()
  {
    return this.toString();
  }
  
  
  public String toString()
  {
    String str = ";";
    for(Student student: students)
      str += student.toString();

    for (int index = 0; index < students.size(); index++)
    {
      str += students.get(index).toString();
    }
    
    str += "Class Average = " +this.average();
    
    return str;
  }
}
