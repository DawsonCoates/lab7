
public class SecureCoin implements Lockable
{
  private int key = -1;
  private final int HEADS = 0;
  private final int TAILS = 1;

  private int face;

  //-----------------------------------------------------------------
  //  Sets up the coin by flipping it initially.
  //-----------------------------------------------------------------
  public SecureCoin()
  {
      flip();
  }
  
  
  public void flip()
  {
    if (this.locked())  
      face = (int) (Math.random() * 2);
    else
      System.out.println("Coin is locked - No action");
  }
  public boolean setKey(int key)
  {
    // TODO Auto-generated method stub
    return false;
  }

  public boolean lock(int key)
  {
    // TODO Auto-generated method stub
    return false;
  }

  public boolean unlock(int key)
  {
    // TODO Auto-generated method stub
    return false;
  }

  public boolean locked()
  {
    // TODO Auto-generated method stub
    return false;
  }

}
