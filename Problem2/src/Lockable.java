
public interface Lockable
{
  //seyKey, lock, unlock and locked.
  public boolean setKey(int key);
  public boolean lock(int key);
  public boolean unlock(int key);
  public boolean locked();
}
