
public interface Priority
{
  public abstract int getPriority();
  public void setPriority(int priority);
}
