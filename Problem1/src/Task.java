
public class Task implements Priority
{
  private int priority;
  private String description;
  
  public int getPriority()
  {
    return this.priority;
  }
  
  public void setPriority(int priority)
  {
    this.priority = priority;
  }
  
}
