
public class Utility
{

    public double average(int one, int two)
    {
      final double TWO = 2.0;
      return (one + two) / TWO;
    }
    
    public double average(int one, int two, int three)
    {
      final double THREE = 3.0;
      return (one + two + three) / THREE;
    }
    
    public double average(int one, int two, int three, int four)
    {
      final double FOUR = 4.0;
      return (one + two + three + four) / FOUR;
    }
}
