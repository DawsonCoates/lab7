//Exercise7.1
//Write a method called average that accepts two integer parameters and returns 
//their average as a floating point value.
//Exercise7.2
//Overload the average method of Exercise 7.1 such that if three integers are 
//provided as parameters, the method returns the average of all three.
//Exercise7.3
//Overload the average method of Exercise7.1 to accept four integer parameters 
//and return their average.

public class Driver
{

  public static void main(String[] args)
  {
    Utility util = new Utility();
    int uno = 1, duo = 2, trio = 3, quadro = 4;
    
    System.out.println(util.average(uno, duo));
    System.out.println(util.average(uno, duo, trio));
    System.out.println(util.average(uno, duo, trio, quadro));
  }

}
